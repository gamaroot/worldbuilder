﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraInput : MonoBehaviour
{
    [SerializeField] private float panSpeed = 0.1f;

    private bool cameraPanEnabled = true;

#if !UNITY_ANDROID || !UNITY_IOS
    private Vector3 lastInputPosition;
#endif

    private void Awake()
    {
        Input.multiTouchEnabled = false;
    }

    private void Update()
    {
        this.HandleInput();
    }

    private void HandleInput()
    {
        Vector2 inputDeltaPosition = Vector2.zero;

#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (this.IsPointerOverUI())
                {
                    this.cameraPanEnabled = false;
                }
            }
            else if (this.cameraPanEnabled && touch.phase == TouchPhase.Moved)
            {
                inputDeltaPosition = Input.GetTouch(0).deltaPosition;
        
                this.TranslateCamera(inputDeltaPosition);
            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                this.cameraPanEnabled = true;
            }
        }
#else
        if (Input.GetMouseButtonDown(0))
        {
            if (this.IsPointerOverUI())
            {
                this.cameraPanEnabled = false;
            }
            else
            {
                this.lastInputPosition = Input.mousePosition;
            }
        }
        else if (this.cameraPanEnabled && Input.GetMouseButton(0))
        {
            inputDeltaPosition = Input.mousePosition - this.lastInputPosition;

            this.TranslateCamera(inputDeltaPosition);

            this.lastInputPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            this.cameraPanEnabled = true;
        }
#endif
    }

    private void TranslateCamera(Vector2 inputDeltaPosition)
    {
        base.transform.Translate(-inputDeltaPosition.x * this.panSpeed, -inputDeltaPosition.y * this.panSpeed, 0);
    }

    private bool IsPointerOverUI()
    {
#if UNITY_ANDROID || UNITY_IOS
        return EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId);
#else
        return EventSystem.current.IsPointerOverGameObject();
#endif
    }
}
