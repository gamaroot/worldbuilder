﻿public class URL
{
    public const string AUTH_LOGIN_URL = PUSH_START_DEV_API_BASE_URL + "auth/login";
    public const string AUTH_LOGOUT_URL = PUSH_START_DEV_API_BASE_URL + "auth/logout";

    public const string PLAYER_STATUS_URL = PUSH_START_DEV_API_BASE_URL + "status";

    private const string PUSH_START_DEV_API_BASE_URL = "http://dev.pushstart.com.br/desafio/public/api/";
}