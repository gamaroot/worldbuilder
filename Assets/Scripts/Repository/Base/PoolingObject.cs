﻿using UnityEngine;

public class PoolingObject : MonoBehaviour
{
    private ResourcePool resourcePool;
    
    private void OnEnable()
    {
        this.transform.SetParent(this.resourcePool.GetParent());
    }

    public void ScheduleAutoDisableInSeconds(float time)
    {
        base.Invoke("Disable", time);
    }

    public void SetResourcePool(ResourcePool resourcePool)
    {
        this.resourcePool = resourcePool;
    }

    public void Disable()
    {
        this.OnPoolingObjectDisable();

        base.gameObject.SetActive(false);
    }

    private void OnPoolingObjectDisable()
    {
        if (this.resourcePool != null)
        {
            this.resourcePool.SendBackToThePool(base.gameObject);
        }
    }
}