﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ResourcePool
{
    public int PoolSize = 10;
    public int ExpandedPoolSize = 10;
    
    [SerializeField] private GameObject resourcePrefab;

    private Stack<GameObject> Pool = new Stack<GameObject>();

    private Transform parentGameObject;

    public ResourcePool(Transform parentGameObject, GameObject resourcePrefab, int poolSize = 10)
    {
        GameObject node = new GameObject(resourcePrefab.name);
        node.transform.SetParent(parentGameObject);

        this.parentGameObject = node.transform;
        this.resourcePrefab = resourcePrefab;
        this.PoolSize = poolSize;

        this.AddObjectsToPool(PoolSize);
    }

    public T BorrowMeObjectFromPool<T>(float expirationTimeInSeconds = -1f)
    {
        if (this.Pool.Count == 0)
        {
            this.ExpandPoolSize();
        }

        GameObject hereToYou = this.Pool.Pop();

        if (expirationTimeInSeconds > 0)
        {
            PoolingObject poolingObject = hereToYou.GetComponent<PoolingObject>();
            poolingObject.ScheduleAutoDisableInSeconds(expirationTimeInSeconds);
        }

        hereToYou.SetActive(true);

        return hereToYou.GetComponent<T>();
    }

    public void SendBackToThePool(GameObject giveMeBack)
    {
        giveMeBack.transform.SetParent(this.parentGameObject, true);

        this.Pool.Push(giveMeBack);
    }

    public Transform GetParent()
    {
        return this.parentGameObject;
    }
    
    private void AddObjectsToPool(int quantity)
    {
        for (int i = 0; i < quantity; i++)
        {
            GameObject newResourceObject = MonoBehaviour.Instantiate(this.resourcePrefab);
            newResourceObject.name += i;
            newResourceObject.transform.SetParent(this.parentGameObject, true);
            newResourceObject.SetActive(false);

            PoolingObject poolingObject = newResourceObject.AddComponent<PoolingObject>();
            poolingObject.SetResourcePool(this);

            this.Pool.Push(newResourceObject);
        }
    }

    private void ExpandPoolSize()
    {
        this.AddObjectsToPool(ExpandedPoolSize);
    }
}