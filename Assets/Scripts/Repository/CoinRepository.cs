﻿using UnityEngine;

public class CoinRepository : MonoBehaviour
{
    [SerializeField] private Coin goldenCoinPrefab;

    private static ResourcePool goldenCoinResourcePool;

    private void Awake()
    {
        goldenCoinResourcePool = new ResourcePool(base.transform, this.goldenCoinPrefab.gameObject, 5);

        MonoBehaviour.DontDestroyOnLoad(this);
    }

    public static Coin LoadGoldenCoin()
    {
        return goldenCoinResourcePool.BorrowMeObjectFromPool<Coin>();
    }
}
