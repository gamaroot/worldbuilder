﻿using UnityEngine;

public class BuildingRepository : MonoBehaviour
{
    [SerializeField] private Building factoryPrefab;
    [SerializeField] private Building farmPrefab;
    [SerializeField] private Building housesPrefab;
    [SerializeField] private Building mallPrefab;
    [SerializeField] private Building parkPrefab;

    private static ResourcePool factoryResourcePool;
    private static ResourcePool farmResourcePool;
    private static ResourcePool housesResourcePool;
    private static ResourcePool mallResourcePool;
    private static ResourcePool parkResourcePool;

    private void Awake()
    {
        factoryResourcePool = new ResourcePool(base.transform, this.factoryPrefab.gameObject, 5);
        farmResourcePool    = new ResourcePool(base.transform, this.farmPrefab.gameObject, 1);
        housesResourcePool  = new ResourcePool(base.transform, this.housesPrefab.gameObject, 1);
        mallResourcePool    = new ResourcePool(base.transform, this.mallPrefab.gameObject, 1);
        parkResourcePool    = new ResourcePool(base.transform, this.parkPrefab.gameObject, 1);

        MonoBehaviour.DontDestroyOnLoad(this);
    }

    public static Building LoadBuilding(BuildingType type)
    {
        Building building = null;

        switch (type)
        {
            case BuildingType.FACTORY:

                building = factoryResourcePool.BorrowMeObjectFromPool<Building>();
                break;

            case BuildingType.FARM:

                building = farmResourcePool.BorrowMeObjectFromPool<Building>();
                break;

            case BuildingType.HOUSES:

                building = housesResourcePool.BorrowMeObjectFromPool<Building>();
                break;

            case BuildingType.MALL:

                building = mallResourcePool.BorrowMeObjectFromPool<Building>();
                break;

            case BuildingType.PARK:

                building = parkResourcePool.BorrowMeObjectFromPool<Building>();
                break;

            default:
                Debug.LogErrorFormat("Trying to create a Building with an unknown type: {0}", building.Type);
                break;
        }

        return building;
    }
}
