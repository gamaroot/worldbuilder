﻿using UnityEngine;

[RequireComponent(typeof(UserSessionHandler))]
public class GameHandler : MonoBehaviour
{
    [SerializeField] private GameScreenViewHandler gameScreenViewHandler;
    
    [SerializeField] private ScreenNavigator screenNavigator;

    [SerializeField] private Transform totalCoinsHUD;

    private CoinAnimator coinAnimator;
    
    private UserSessionHandler userSessionHandler;
    
    private bool gamePaused;

    private void Awake()
    {
        this.coinAnimator = new CoinAnimator();

        this.userSessionHandler = base.GetComponent<UserSessionHandler>();
    }
    
    public void StartGame(PlayerStatus playerStatus)
    {
        Wallet.Instance.SetOnWalletChangeListener(this.UpdateTotalCoinsDisplay);

        Wallet.Instance.Initialize(playerStatus.money);

        this.gameScreenViewHandler.UsernameText = playerStatus.nickname;
    }

    public void QuitGame()
    {
        this.ResumeGame();

        this.userSessionHandler.Logout();

        this.screenNavigator.GoFromGameToLoginScreen();
    }

    public void PauseGame()
    {
        this.gameScreenViewHandler.HidePauseButton();
        this.gameScreenViewHandler.ShowResumeButton();

        this.gamePaused = true;

        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        this.gameScreenViewHandler.HideResumeButton();
        this.gameScreenViewHandler.ShowPauseButton();

        this.gamePaused = false;

        Time.timeScale = 1f;
    }

    public bool IsGamePaused()
    {
        return this.gamePaused;
    }

    protected void ConstructBuilding(BuildingType type, Vector2 spawnPoint)
    {
        Building building = BuildingRepository.LoadBuilding(type);

        building.transform.position = spawnPoint;

        this.SpendCoin(spawnPoint, building.ConstructionValue);
    }

    protected void AdquireCoin(Vector3 spawnPoint, int value)
    {
        Coin coin = CoinRepository.LoadGoldenCoin();
        coin.Value = value;

        this.coinAnimator.StartAdquireCoinAnimation(coin, spawnPoint, totalCoinsHUD.position);

        Wallet.Instance.Add(value);
    }

    private void SpendCoin(Vector3 spawnPoint, int value)
    {
        Coin coin = CoinRepository.LoadGoldenCoin();
        coin.Value = value;

        this.coinAnimator.StartSpendCoinAnimation(coin, spawnPoint);
        
        Wallet.Instance.Subtract(value);
    }

    private void UpdateTotalCoinsDisplay(int coins)
    {
        this.gameScreenViewHandler.TotalCoinsText = coins.ToString();//CurrencyFormattingUtils.GetFormattedCoinValueText(coins);
    }
}
