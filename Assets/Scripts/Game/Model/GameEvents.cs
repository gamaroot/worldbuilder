﻿using UnityEngine;

public class GameEvents : GameHandler
{
    public void OnGameStart(PlayerStatus playerStatus)
    {
        base.StartGame(playerStatus);
    }

    public void OnBuildingDropSuccessfully(BuildingType type, Vector2 spawnPoint)
    {
        base.ConstructBuilding(type, spawnPoint);
    }

    public void OnQuitGame()
    {
        base.QuitGame();
    }

    public void OnPause()
    {
        base.PauseGame();
    }

    public void OnResume()
    {
        base.ResumeGame();
    }
}