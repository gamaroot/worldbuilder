﻿public class Coin : CoinViewHandler
{
    public int Value
    {
        get
        {
            return this.value;
        }

        set
        {
            this.value = value;

            string formattedCoinValue = CurrencyFormattingUtils.GetFormattedCoinValueText(this.value);

            base.CoinValueText.text = formattedCoinValue;
        }
    }

    private int value;
}
