﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DraggableBuilding : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private GameEvents gameEvents;

    [SerializeField] private LayerMask terrainLayerMask;
    [SerializeField] private LayerMask uiLayerMask;
    [SerializeField] private LayerMask buildingLayerMask;

    [Header("Building Settings")]
    [SerializeField] private BuildingType buildingType;
    [SerializeField] private float deploymentCooldown = 5f;

    private Vector2 originalPoint;

    private Image buildingImage;

    private BuildingDraggingAnimator buildingDraggingAnimator;

    private void Awake()
    {
        this.buildingImage = base.GetComponent<Image>();
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (this.IsDragAvailable())
        {
            this.originalPoint = base.transform.position;

            if (buildingDraggingAnimator == null)
            {
                Image buildingAuxliaryImage = base.GetComponentsInChildren<Image>()[1];
                
                this.buildingDraggingAnimator = new BuildingDraggingAnimator(this.buildingImage, buildingAuxliaryImage, this.originalPoint, this.deploymentCooldown);
            }
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (this.IsDragAvailable())
        {
            base.transform.position = eventData.position;

            bool dropAvailable = this.IsDropAvailable(eventData.position);

            BuildingDraggingAnimationState animationState = dropAvailable ? BuildingDraggingAnimationState.DROP_AVAILABLE :
                                                                            BuildingDraggingAnimationState.DROP_UNAVAILABLE;

            this.buildingDraggingAnimator.SetDraggingAnimationState(animationState);
        }
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if (this.IsDragAvailable())
        {
            Vector2 screenPoint = eventData.position;

            bool dropAvailable = this.IsDropAvailable(screenPoint);

            BuildingDraggingAnimationState animationState = dropAvailable ? BuildingDraggingAnimationState.DROP_SUCCESSFULL :
                                                                            BuildingDraggingAnimationState.DROP_FAILED;

            this.buildingDraggingAnimator.SetDraggingAnimationState(animationState);

            if (animationState == BuildingDraggingAnimationState.DROP_SUCCESSFULL)
            {
                this.gameEvents.OnBuildingDropSuccessfully(this.buildingType, Camera.main.ScreenToWorldPoint(screenPoint));
            }
        }
    }

    private bool IsDragAvailable()
    {
        return this.buildingImage.fillAmount == 1f;
    }

    private bool IsDropAvailable(Vector2 screenPoint)
    {
        return this.IsHitting2dObject(screenPoint, this.terrainLayerMask) &&
               !this.IsHitting2dObject(screenPoint, this.uiLayerMask) &&
               !this.IsHitting2dObject(screenPoint, this.buildingLayerMask);
    }

    private bool IsHitting2dObject(Vector2 screenPoint, LayerMask layerMask)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPoint);

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, float.MaxValue, layerMask);

        Transform hitTransfom = null;
        if (hit.collider)
        {
            hitTransfom = hit.transform;
        }

        return hitTransfom != null;
    }
}
