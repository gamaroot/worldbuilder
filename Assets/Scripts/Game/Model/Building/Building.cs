﻿using DG.Tweening;
using UnityEngine;

public class Building : MonoBehaviour
{
    public BuildingType Type;

    public Vector2 BuildingArea;

    public int ConstructionValue;

    public int IncomingRatePerSecond;

    private void Start()
    {
        this.ShowSpawnAnimation();
    }

    private void OnEnable()
    {
        InvokeRepeating("Produce", 0, 1f);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Produce()
    {
        Wallet.Instance.Add(this.IncomingRatePerSecond);
    }

    private void ShowSpawnAnimation()
    {
        TweenParams tweenParams = new TweenParams();
        tweenParams.SetEase(Ease.OutElastic);

        base.transform.localScale = Vector3.zero;
        base.transform.DOScale(1f, 1f).SetAs(tweenParams);
    }
}
