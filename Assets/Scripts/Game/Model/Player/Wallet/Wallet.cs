﻿using System;

public class Wallet
{
    private int coins;

    private Action<int> onWalletChangeListener;

    private static Wallet instance;

    private Wallet() { }

    public static Wallet Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Wallet();
            }
            return instance;
        }
    }

    public void Initialize(int currenTotaltCoins)
    {
        this.coins = currenTotaltCoins;

        this.onWalletChangeListener(this.coins);
    }

    public void Add(int value)
    {
        this.coins += value;

        this.onWalletChangeListener(this.coins);
    }

    public void Subtract(int value)
    {
        this.coins -= value;

        this.onWalletChangeListener(this.coins);
    }

    public void SetOnWalletChangeListener(Action<int> listener)
    {
        this.onWalletChangeListener = listener;
    }
}
