﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class CoinViewHandler : MonoBehaviour
{
    [HideInInspector] public SpriteRenderer CoinSpriteRenderer;
    public TextMesh CoinValueText;

    private void Awake()
    {
        this.CoinSpriteRenderer = base.GetComponent<SpriteRenderer>();
    }
}
