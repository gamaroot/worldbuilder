﻿using UnityEngine;

public class BuildingViewHandler : MonoBehaviour
{
    [SerializeField] private SpriteRenderer buildingSpriteRenderer;

    public void SetBuildingSprite(Sprite sprite)
    {
        this.buildingSpriteRenderer.sprite = sprite;
    }
}
