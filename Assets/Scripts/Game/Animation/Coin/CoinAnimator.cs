﻿using DG.Tweening;
using UnityEngine;

public class CoinAnimator
{
    public void StartAdquireCoinAnimation(Coin coin, Vector2 initialPoint, Vector2 destinationPoint)
    {
        coin.transform.DOMove(destinationPoint, 0.3f);

        coin.CoinSpriteRenderer.DOFade(0, 0.5f)
                                .OnComplete(() => this.OnAnimationEnd(coin));
    }

    public void StartSpendCoinAnimation(Coin coin, Vector2 initialPoint)
    {
        Vector2 destinationPoint = initialPoint;
        destinationPoint.y += 2.5f;

        coin.transform.position = initialPoint;

        coin.transform.DOMove(destinationPoint, 1f);

        coin.CoinSpriteRenderer.DOFade(0, 1.25f)
                                .OnComplete(() => this.OnAnimationEnd(coin));
    }

    private void OnAnimationEnd(Coin coin)
    {
        coin.gameObject.SetActive(false);
    }
}
