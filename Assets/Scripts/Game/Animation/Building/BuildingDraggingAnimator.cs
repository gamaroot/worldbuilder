﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BuildingDraggingAnimator
{
    private Image buildingImage;
    private Image buildingAuxliaryImage;

    private Vector3 originalPoint;

    private float cooldown;

    private BuildingDraggingAnimationState currentAnimationState;

    public BuildingDraggingAnimator(Image buildingImage, Image buildingAuxliaryImage, Vector2 originalPoint, float cooldown)
    {
        this.buildingImage = buildingImage;
        this.buildingAuxliaryImage = buildingAuxliaryImage;

        this.originalPoint = originalPoint;

        this.cooldown = cooldown;
    }
    
    public void SetDraggingAnimationState(BuildingDraggingAnimationState newState)
    {
        switch (newState)
        {
            case BuildingDraggingAnimationState.DROP_AVAILABLE:

                if (newState != this.currentAnimationState)
                {
                    this.currentAnimationState = newState;

                    this.DoBlinkColor(Color.green);
                }
                break;

            case BuildingDraggingAnimationState.DROP_UNAVAILABLE:

                if (newState != this.currentAnimationState)
                {
                    this.currentAnimationState = newState;

                    this.DoBlinkColor(Color.red);
                }
                break;

            case BuildingDraggingAnimationState.DROP_FAILED:

                this.SendBackToOriginalState();
                break;

            case BuildingDraggingAnimationState.DROP_SUCCESSFULL:

                TweenCallback fadeOutCompleteAction = () =>
                {
                    this.SetBuildingImageToOriginalState();

                    this.EnterInCooldownState();
                };

                this.FadeOutToOriginalState(fadeOutCompleteAction);
                
                break;

            default:
                break;
        }
    }

    private void EnterInCooldownState()
    {
        this.buildingImage.fillAmount = 0;

        TweenCallback cooldownCompleteAction = () =>
        {
            Color newColor = this.buildingAuxliaryImage.color;
            newColor.a = 0f;

            this.buildingAuxliaryImage.color = newColor;
        };
        
        TweenCallback showAuxiliaryImageCompleteAction = () => this.buildingImage.DOFillAmount(1f, this.cooldown).OnComplete(cooldownCompleteAction);

        this.buildingAuxliaryImage.DOFade(0.3f, 0.3f).OnComplete(showAuxiliaryImageCompleteAction);
    }

    private void DoBlinkColor(Color toColor)
    {
        this.buildingImage.DOKill();

        TweenParams tweenParams = new TweenParams();
        tweenParams.SetLoops(-1, LoopType.Yoyo);
        
        this.buildingImage.DOColor(toColor, 1f).SetAs(tweenParams);
    }
    
    private void SendBackToOriginalState()
    {
        this.buildingImage.DOKill();

        this.buildingImage.DOColor(Color.white, 1f);

        this.buildingImage.transform.DOMove(this.originalPoint, 0.3f);

        this.currentAnimationState = BuildingDraggingAnimationState.DROP_FAILED;
    }

    private void FadeOutToOriginalState(TweenCallback completeAction)
    {
        this.buildingImage.DOKill();

        this.buildingImage.DOColor(Color.white, 0.6f);

        this.buildingImage.DOFade(0, 0.3f).OnComplete(completeAction);
    }

    private void SetBuildingImageToOriginalState()
    {
        this.buildingImage.transform.position = this.originalPoint;

        this.buildingImage.color = Color.white;
    }
}
