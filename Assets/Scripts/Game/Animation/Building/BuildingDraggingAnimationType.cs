﻿public enum BuildingDraggingAnimationState
{
    DROP_FAILED,
    DROP_SUCCESSFULL,
    DROP_AVAILABLE,
    DROP_UNAVAILABLE
}
