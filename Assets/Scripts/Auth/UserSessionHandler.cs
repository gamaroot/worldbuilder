﻿using UnityEngine;
using System;
using System.Text;
using UnityEngine.Networking;
using System.Collections.Generic;

public class UserSessionHandler : HttpRequester
{
    private string authToken;

    public void ValidateLoginCredentials(CryptedLoginCredentials cryptedLoginCredentials, Action<bool, string> loginValidationListener)
    {
        string loginJson = JsonUtility.ToJson(cryptedLoginCredentials.CryptedCredentials);

        Action<string, long> responseListener = (string response, long httpCode) =>
        {
            bool validLogin = false;

            string errorMessage = "";

            switch (httpCode)
            {
                case 200:
                    LoginResponse loginResponse = JsonUtility.FromJson<LoginResponse>(response);

                    this.authToken = loginResponse.token;

                    validLogin = true;

                    break;

                case 400:
                    errorMessage = "Ops, there was a problem with your request. Try again.";
                    break;

                case 401:
                    errorMessage = "Invalid credentials.";
                    break;

                case 403:
                    errorMessage = "Sorry, something went really wrong... Just try again :(";
                    break;

                default:
                    errorMessage = "There's something wrong with your device. A team of highly trained monkeys has been dispatched to deal with this situation.";
                    break;
            }

            loginValidationListener(validLogin, errorMessage);
        };

        Dictionary<string, string> httpHeaders = new Dictionary<string, string>();
        httpHeaders.Add("Content-Type", "application/json");

        WebRequestData webRequestData = new WebRequestData();
        webRequestData.Url = URL.AUTH_LOGIN_URL;
        webRequestData.RequestMethod = UnityWebRequest.kHttpVerbPOST;

        webRequestData.HttpHeaders = httpHeaders;

        webRequestData.RawData = Encoding.UTF8.GetBytes(loginJson);

        webRequestData.ResponseListener = responseListener;

        base.StartCoroutine(base.SendWebRequest(webRequestData));
    }

    public void LoadPlayerStatus(Action<PlayerStatus> onPlayerStatusLoad)
    {
        Action<string, long> responseListener = (string response, long httpCode) =>
        {
            switch (httpCode)
            {
                case 200:
                    PlayerStatus playerStatus = JsonUtility.FromJson<PlayerStatus>(response);

                    onPlayerStatusLoad(playerStatus);

                    break;

                case 401:
                    Debug.LogWarningFormat("Unauthorized: httpCode-{0}", httpCode);

                    break;

                default:

                    Debug.LogWarningFormat("Something went wrong: httpCode-{0}", httpCode);
                    break;
            }
        };

        Dictionary<string, string> httpHeaders = new Dictionary<string, string>();
        httpHeaders.Add("X-Authorization", this.authToken);

        WebRequestData webRequestData = new WebRequestData();
        webRequestData.Url = URL.PLAYER_STATUS_URL;
        webRequestData.RequestMethod = UnityWebRequest.kHttpVerbGET;

        webRequestData.HttpHeaders = httpHeaders;

        webRequestData.ResponseListener = responseListener;

        base.StartCoroutine(base.SendWebRequest(webRequestData));
    }

    public void Logout()
    {
        Dictionary<string, string> httpHeaders = new Dictionary<string, string>();
        httpHeaders.Add("X-Authorization", this.authToken);

        WebRequestData webRequestData = new WebRequestData();
        webRequestData.Url = URL.AUTH_LOGOUT_URL;
        webRequestData.RequestMethod = UnityWebRequest.kHttpVerbDELETE;

        webRequestData.HttpHeaders = httpHeaders;
        
        base.StartCoroutine(base.SendWebRequest(webRequestData));
    }
}
