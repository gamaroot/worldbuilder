﻿using System.Security.Cryptography;
using System.Text;

public class CryptedLoginCredentials
{
    public LoginCredentials CryptedCredentials
    {
        get { return cryptedCredentials; }

        private set
        {
            using (SHA256 sha256 = SHA256Managed.Create())
            {
                byte[] bytes = new UTF8Encoding().GetBytes(value.password);

                byte[] hashBytes = sha256.ComputeHash(bytes);

                string hashAsString = "";
                
                for (int index = 0; index < hashBytes.Length; index++)
                {
                    hashAsString += System.Convert.ToString(hashBytes[index], 16).PadLeft(2, '0');
                }

                this.cryptedCredentials = value;
                this.cryptedCredentials.password = hashAsString.ToString();
            }
        }
    }

    private LoginCredentials cryptedCredentials;

    public CryptedLoginCredentials(string username, string password)
    {
        this.CryptedCredentials = new LoginCredentials(username, password);
    }
}
