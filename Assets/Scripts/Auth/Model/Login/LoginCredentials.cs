﻿public class LoginCredentials
{
    public string username;
    public string password;

    public LoginCredentials(string username, string password)
    {
        this.username = username;
        this.password = password;
    }
}
