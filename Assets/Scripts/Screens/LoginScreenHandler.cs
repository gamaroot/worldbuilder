﻿using System;
using UnityEngine;

[RequireComponent(typeof(LoginScreenViewHandler))]
public class LoginScreenHandler : MonoBehaviour
{
    [SerializeField] private UserSessionHandler userSessionHandler;

    [SerializeField] private GameEvents gameEvents;

    [SerializeField] private ScreenNavigator screenNavigator;
    
    private LoginScreenViewHandler loginScreenViewHandler;

    private void Awake()
    {
        this.loginScreenViewHandler = base.GetComponent<LoginScreenViewHandler>();
    }

    public void Login()
    {
        string username = loginScreenViewHandler.UsernameInput;
        string password = this.loginScreenViewHandler.PasswordInput;

        if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
        {
            this.loginScreenViewHandler.ShowErrorMessage("Please provide an username and password.");
        }
        else if (!NetworkTester.IsInternetConnectionAvailable())
        {
            this.loginScreenViewHandler.ShowErrorMessage("No Internet connection.");
        }
        else
        {
            CryptedLoginCredentials cryptedLoginCredentials = new CryptedLoginCredentials(username, password);

            Action<bool, string> onLoginResponse = (bool valid, string errorMessage) =>
            {
                if (valid)
                {
                    this.OnLoginValidate();
                }
                else
                {
                    this.loginScreenViewHandler.ShowErrorMessage(errorMessage);
                }
            };

            this.userSessionHandler.ValidateLoginCredentials(cryptedLoginCredentials, onLoginResponse);
        }
    }

    private void OnLoginValidate()
    {
        Action<PlayerStatus> onPlayerStatusLoad = (PlayerStatus playerStatus) =>
        {
            Action onNavigationAnimationEnd = () => this.gameEvents.OnGameStart(playerStatus);

            this.screenNavigator.GoFromLoginToGameScreen(onNavigationAnimationEnd);
        };

        this.userSessionHandler.LoadPlayerStatus(onPlayerStatusLoad);
    }
}
