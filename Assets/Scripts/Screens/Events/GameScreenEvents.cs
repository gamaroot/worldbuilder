﻿using UnityEngine;

public class GameScreenEvents : MonoBehaviour
{
    [SerializeField] private GameEvents gameEvents;

    public void OnQuitGameButtonClick()
    {
        this.gameEvents.OnQuitGame();
    }

    public void OnPauseButtonClick()
    {
        this.gameEvents.OnPause();
    }

    public void OnResumeButtonClick()
    {
        this.gameEvents.OnResume();
    }
}
