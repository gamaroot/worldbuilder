﻿using System;

public class ScreenNavigator : ScreenNavigationAnimator
{
    public void GoFromLoginToGameScreen(Action animationEndAction = null)
    {
        base.SetAnimationEndListener(animationEndAction);
        base.OnGoFromLoginToGameScreen();
    }

    public void GoFromGameToLoginScreen(Action animationEndAction = null)
    {
        base.SetAnimationEndListener(animationEndAction);
        base.OnGoFromGameToLoginScreen();
    }
}
