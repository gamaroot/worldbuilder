﻿public enum ScreenAnimationLayer
{
    SHOW_SCREEN = 0,
    HIDE_SCREEN = 1,
    SHOW_LAYERED_SCREEN = 2,
    HIDE_LAYERED_SCREEN = 3
}
