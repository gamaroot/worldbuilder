﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ScreenNavigationAnimator : MonoBehaviour
{
    private Animator screenAnimatorController;
    
    private Action animationEndObserver;

    private void Awake()
    {
        this.screenAnimatorController = base.GetComponent<Animator>();
    }
    
    public void SetAnimationEndListener(Action action)
    {
        this.animationEndObserver = action;
    }
    
    public void OnGoFromLoginToGameScreen()
    {
        this.StartScreenTransition(ScreenID.LOGIN, ScreenID.GAME);
    }
    
    public void OnGoFromGameToLoginScreen()
    {
        this.StartScreenTransition(ScreenID.GAME, ScreenID.LOGIN);
    }
    
    // Called by Animator
    public void OnNavigationAnimationEnd()
    {
        this.InvokeAnimationEndListener();
    }

    private void StartScreenTransition(ScreenID screenToBeHide, ScreenID screenToBeShow)
    {
        this.screenAnimatorController.Play(screenToBeShow.ToString(), (int)ScreenAnimationLayer.SHOW_SCREEN);
        this.screenAnimatorController.Play(screenToBeHide.ToString(), (int)ScreenAnimationLayer.HIDE_SCREEN);
    }

    private void StartLayeredScreenTransition(ScreenID screenToBeHide, ScreenID screenToBeShow)
    {
        this.screenAnimatorController.Play(screenToBeShow.ToString(), (int)ScreenAnimationLayer.SHOW_LAYERED_SCREEN);
        this.screenAnimatorController.Play(screenToBeHide.ToString(), (int)ScreenAnimationLayer.HIDE_LAYERED_SCREEN);
    }
    
    private void InvokeAnimationEndListener()
    {
        if (this.animationEndObserver != null)
        {
            this.animationEndObserver.Invoke();
        }
        this.animationEndObserver = null;
    }
}
