﻿using UnityEngine;
using UnityEngine.UI;

public class GameScreenViewHandler : MonoBehaviour
{
    [SerializeField] private Text usernameText;
    [SerializeField] private Text totalCoinsText;

    [SerializeField] private Button pauseButton;
    [SerializeField] private Button resumeButton;

    public string UsernameText
    {
        get { return this.usernameText.text; }
        
        set { this.usernameText.text = value; }
    }

    public string TotalCoinsText
    {
        get { return this.totalCoinsText.text; }

        set { this.totalCoinsText.text = value; }
    }

    public void HidePauseButton()
    {
        this.pauseButton.gameObject.SetActive(false);
    }

    public void HideResumeButton()
    {
        this.resumeButton.gameObject.SetActive(false);
    }

    public void ShowPauseButton()
    {
        this.pauseButton.gameObject.SetActive(true);
    }

    public void ShowResumeButton()
    {
        this.resumeButton.gameObject.SetActive(true);
    }

}
