﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LoginScreenViewHandler : MonoBehaviour, ISelectHandler
{
    [SerializeField] private InputField usernameInputField;
    [SerializeField] private InputField passwordInputField;

    [SerializeField] private Text loginErrorText;

    private void Start()
    {
        this.usernameInputField.Select();
        this.usernameInputField.ActivateInputField();

        this.usernameInputField.onValueChanged.AddListener(
            delegate { this.ClearErrorMessage(); }
        );

        this.passwordInputField.onValueChanged.AddListener(
            delegate { this.ClearErrorMessage(); }
        );
    }

    public void OnSelect(BaseEventData eventData)
    {
        this.ClearErrorMessage();
    }

    public string UsernameInput
    {
        get { return this.usernameInputField.text; }
    }

    public string PasswordInput
    {
        get { return this.passwordInputField.text; }
    }
    
    public void ShowErrorMessage(string message)
    {
        this.loginErrorText.text = message;
    }

    private void ClearErrorMessage()
    {
        this.loginErrorText.text = "";
    }
}
