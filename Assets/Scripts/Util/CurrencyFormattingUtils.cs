﻿using UnityEngine;

public class CurrencyFormattingUtils : MonoBehaviour
{
    public static string GetFormattedCoinValueText(float value)
    {
        string formattedValueText = "";

        float valueSign = Mathf.Sign(value);

        float absValue = Mathf.Abs(value);

        if (absValue >= 1000f && absValue < 1000000f)
        {
            formattedValueText = (int)(absValue * valueSign / 1000f) + "K";
        }
        else if (absValue >= 1000000f && absValue < 1000000000f)
        {
            formattedValueText = (int)(absValue * valueSign / 1000000f) + "M";
        }
        else if (absValue >= 1000000000f)
        {
            formattedValueText = (int)(absValue * valueSign / 1000000000f) + "B";
        }
        else
        {
            formattedValueText = absValue.ToString();
        }

        return formattedValueText;
    }
}
