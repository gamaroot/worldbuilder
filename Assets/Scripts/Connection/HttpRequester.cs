﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class HttpRequester : MonoBehaviour
{
    public IEnumerator SendWebRequest(WebRequestData webRequestData)
    {
        Guid requestGuid = Guid.NewGuid();

        Debug.LogFormat("REQUEST #{0:N}:\nURL: {1}\nMETHOD: {2}", requestGuid, webRequestData.Url, webRequestData.RequestMethod);

        UnityWebRequest unityWebRequest = new UnityWebRequest(webRequestData.Url, webRequestData.RequestMethod);

#if UNITY_WEBGL
        unityWebRequest.SetRequestHeader("Access-Control-Allow-Origin", "*");
        unityWebRequest.SetRequestHeader("Access-Control-Allow-Headers", "Origin, X-Request-Width, Content-Type, Accept");
#endif

        foreach (KeyValuePair<string, string> headerField in webRequestData.HttpHeaders)
        {
            unityWebRequest.SetRequestHeader(headerField.Key, headerField.Value);
        }

        unityWebRequest.uploadHandler = new UploadHandlerRaw(webRequestData.RawData);
        unityWebRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return unityWebRequest.SendWebRequest();

        string responseContent = unityWebRequest.downloadHandler.text;
        long responseCode = unityWebRequest.responseCode;

        Debug.LogFormat("RESPONSE #{0:N}:\nURL: {1}\nMETHOD: {2}\nHTTP CODE: {3}\nCONTENT: {4}", 
                        requestGuid, webRequestData.Url, webRequestData.RequestMethod, responseCode, responseContent);

        if (webRequestData.ResponseListener != null) // It can be not set
        {
            webRequestData.ResponseListener(responseContent, responseCode);
        }
    }
}
