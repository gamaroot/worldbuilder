﻿using System;
using System.Collections.Generic;

public class WebRequestData
{
    public string Url;
    public string RequestMethod;

    public Dictionary<string, string> HttpHeaders;

    public byte[] RawData;

    public Action<string, long> ResponseListener;
}
