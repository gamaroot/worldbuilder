﻿using UnityEngine;

public class NetworkTester
{
    public static bool IsInternetConnectionAvailable()
    {
        NetworkReachability networkReachability = Application.internetReachability;

        return networkReachability == NetworkReachability.ReachableViaLocalAreaNetwork ||
               networkReachability == NetworkReachability.ReachableViaCarrierDataNetwork;
    }
}
